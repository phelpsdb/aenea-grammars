# Using This Repository

The grammars in this repo are heavily modified and borrowed from 
tgrosinger (https://github.com/tgrosinger/aenea-grammars) and 
calmofthestorm (https://github.com/dictation-toolbox/aenea-grammars). 

One day I hope to get around to editing this README to get into more detail 
about how to use them, but until then please take a look at the 
dictation-toolbox repos on github for instructions on how to set up a 
hands-free programming environment that can utilize these Dragonfly grammars.

# Installation instructions

If you're wondering how to use these grammars and haven't heard of Dragonfly+Aenea, 
these instructions will take you from a fresh installation of Windows and Ubuntu
to a working voice controlled setup. I actually used the installation instructions
from [here](https://github.com/dictation-toolbox/aenea) because they were more
verbose, but I'll leave these instructions here to for reference. 

If you already have DragonNatSpeaking+Dragonfly+Aenea set up, looking for the grammars
you can skip this section.

## Client

These steps are meant to be performed on a Windows machine with an attached
microphone. I suggest using a virtual machine as it will allow you to save
snapshots as you progress through the steps.

The machine should have about 2GB of ram and two processors.

1. Install [Dragon NaturallySpeaking 13.0](http://www.nuance.com/for-individuals/by-product/dragon-for-pc/index.htm) to default location
2. Take a snapshot of the VM
3. Download and install [Python 2.7.8 for x32](https://www.python.org/downloads/windows/)
4. Run [get-pip.py](https://bootstrap.pypa.io/get-pip.py) to install pip
5. Install the latest release of [pywin32 for Python 2.7](http://sourceforge.net/projects/pywin32/)
6. Install the latest release of [NatLink](http://sourceforge.net/projects/natlink/)
7. Install other dependencies (`python -m pip install dragonfly jsonrpclib pyparsing`)
8. Take another snapshot of the VM
9. Start a cmd window as administrator
10. Navigate to `C:\NatLink\NatLink\confignatlinkvocolaunimacro`
11. Run `start_natlinkconfigfunctions.py` then use the `e` option to enable
12. If problems are encountered, take a look at [this Github](https://github.com/simianhacker/code-by-voice/issues/2) issue for help
13. Copy `aenea/aenea.json.example` to `C:\NatLink\NatLink\MacroSystem` and edit the ip to the ip of the host
14. Copy the dictation client from the client directory to the NatLink directory
15. Disable the dictation window in Dragon so you can use the dictation client

## Server

These instructions were written for an machine running ubuntu 14.04 LTS. (But it works on 16.04 too, as far as I can tell)

1. Go to the server (linux machine) and navigate in the aenea dir to `server/linux_x11`
2. Copy `config.py.example` to `config.py` and edit, setting the ip to 0.0.0.0
3. Install pip on the host machine (`sudo apt-get install python-pip`)
4. Install xsel and xdotool (`sudo apt-get install xsel xdotool`)
5. Use pip to install jsonrpclib, and yapsy (`sudo pip install jsonrpclib yapsy`)

## Starting everything.

1. On the server, start `aenea/server/linux_x11/server_x11.py`
2. On the client, start Dragon Naturally Speaking
3. On the client, start `aenea_client.py`
4. Give the dictation window focus so it captures anything not covered by a grammar

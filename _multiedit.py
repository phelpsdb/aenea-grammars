import aenea
import aenea.vocabulary
import aenea.configuration

from aenea import (
    AeneaContext,
    AppContext,
    Grammar,
    ProxyAppContext,
    NeverContext,
)
from lib.multiedit_shared import (
    RepeatRule,
    LiteralRule,
)

# Multiedit wants to take over dynamic vocabulary management.
MULTIEDIT_TAGS = ['multiedit', 'multiedit.count']
aenea.vocabulary.inhibit_global_dynamic_vocabulary('multiedit', MULTIEDIT_TAGS)

#---------------------------------------------------------------------------
# Create and load this module's grammar.

conf = aenea.configuration.ConfigWatcher(('grammar_config', 'multiedit')).conf

local_disable_setting = conf.get('local_disable_context', None)
local_disable_context = NeverContext()
if local_disable_setting is not None:
    if not isinstance(local_disable_setting, basestring):
        print 'Local disable context may only be a string.'
    else:
        local_disable_context = AppContext(str(local_disable_setting))



proxy_disable_setting = conf.get('proxy_disable_context', None)
proxy_disable_context = NeverContext()
if proxy_disable_setting is not None:
    if isinstance(proxy_disable_setting, dict):
        d = {}
        for k, v in proxy_disable_setting.iteritems():
            d[str(k)] = str(v)
        proxy_disable_context = ProxyAppContext(**d)
    else:
        proxy_disable_context = ProxyAppContext(
            title=str(proxy_disable_setting),
            match='substring',
        )


context = AeneaContext(proxy_disable_context, local_disable_context)

grammar = Grammar('multiedit', context=~context)
grammar.add_rule(RepeatRule(name='a'))
grammar.add_rule(LiteralRule())

grammar.load()


# Unload function which will be called at unload time.
def unload():
    global grammar
    aenea.vocabulary.uninhibit_global_dynamic_vocabulary(
        'multiedit',
        MULTIEDIT_TAGS,
    )
    for tag in MULTIEDIT_TAGS:
        aenea.vocabulary.unregister_dynamic_vocabulary(tag)
    if grammar:
        grammar.unload()
    grammar = None
